package ai.ultimate.backendchallenge;

import ai.ultimate.backendchallenge.domain.IntentReply;
import ai.ultimate.backendchallenge.domain.Message;
import ai.ultimate.backendchallenge.domain.Reply;
import ai.ultimate.backendchallenge.domain.TrainingData;
import ai.ultimate.backendchallenge.domain.client.ClientRequest;
import ai.ultimate.backendchallenge.domain.client.ClientResponse;
import ai.ultimate.backendchallenge.domain.client.Intent;
import ai.ultimate.backendchallenge.exception.UltimateApiException;
import ai.ultimate.backendchallenge.repository.IntentReplyRepository;
import ai.ultimate.backendchallenge.service.client.UltimateApiClient;
import ai.ultimate.backendchallenge.service.impl.IntentReplyServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;

/**
 * The type Backend challenge application tests.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class BackendChallengeApplicationTests {


    @Mock
    private IntentReplyRepository intentReplyRepository;

    @Mock
    private UltimateApiClient ultimateApiClient;

    @Autowired
    @InjectMocks
    private IntentReplyServiceImpl intentReplyService;

    /**
     * Should return same IntentReply.
     *
     * @throws UltimateApiException the ultimate api exception
     */
    @Test
    public void shouldReturnSameIntentReply() throws UltimateApiException {
        String message = "Greeting";
        String botId = "5f74865056d7bb000fcd39ff";
        String apiKey = "825765d4-7f8d-4d83-bb03-9d45ac9c27c0";

        ClientRequest clientRequest = new ClientRequest(botId, message);
        Intent intent1 = new Intent(0.843622624874115d, "Greeting");
        Intent intent2 = new Intent(0.07790736854076385, "Means or need to contact ");
        Intent intent3 = new Intent(0.06398369371891022, "I want to speak with a human");

        ArrayList<Object> entities = new ArrayList<>();

        ClientResponse clientResponse = new ClientResponse(Arrays.asList(intent1, intent2, intent3), entities);
        given(ultimateApiClient.getIntent(apiKey, clientRequest)).willReturn(clientResponse);

        Message intent1_message1 = new Message("6399fd6989984c7b871c6301744b0af5", "Hello");
        Message intent1_message2 = new Message("68bafebc2a2e4843a56a221c2ceb12ed", "Hi");
        Message intent1_message3 = new Message("b2a3208dc801432992812638368e0668", "Good morning!");
        TrainingData intent1_trainingData = new TrainingData(Arrays.asList(intent1_message1, intent1_message2, intent1_message3));

        Reply intent1_reply = new Reply("f35d7e0936a44102bac9cb96c81eec3b", "Hello :) How can I help you?");

        IntentReply intentReply = new IntentReply("34d7831e137a4016a55f98926800a643", "Greeting", "The visitor says hello.", intent1_trainingData, intent1_reply);

        given(intentReplyRepository.findByName(message)).willReturn(intentReply);
        assertNotNull(intentReply);

        IntentReply replyServiceIntentReply = intentReplyService.getIntentReply(botId, message);
        assertNotNull(replyServiceIntentReply);

        assertEquals(replyServiceIntentReply, intentReply);
    }
}