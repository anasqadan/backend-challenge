package ai.ultimate.backendchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * The type Backend challenge application.
 */
@EnableFeignClients
@EnableMongoRepositories
@EnableAutoConfiguration
@SpringBootApplication
public class BackendChallengeApplication {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(BackendChallengeApplication.class, args);
    }

}
