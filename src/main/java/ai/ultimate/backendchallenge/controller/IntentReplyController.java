package ai.ultimate.backendchallenge.controller;


import ai.ultimate.backendchallenge.domain.IntentReply;
import ai.ultimate.backendchallenge.domain.Reply;
import ai.ultimate.backendchallenge.exception.UltimateApiException;
import ai.ultimate.backendchallenge.service.IntentReplyService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Controller.
 */
@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping("/api/replies")
public class IntentReplyController {

    private final IntentReplyService intentReplyService;

    /**
     * Gets response.
     *
     * @param botId   the bot id
     * @param message the message
     * @return the response
     */
    @GetMapping(value = "/")
    public ResponseEntity<String> getIntentReply(@RequestParam(value = "botId") String botId, @RequestParam(value = "message") String message) throws UltimateApiException {

        log.info("Retrieving intent request by the following botId {} and the message is {}", botId, message);

        IntentReply intentReply = intentReplyService.getIntentReply(botId, message);
        if (intentReply == null) {

            log.error("No returned intent matching you message!");
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        Reply reply = intentReply.getReply();
        String replyText = reply.getText();

        return new ResponseEntity(replyText, HttpStatus.OK);
    }

}
