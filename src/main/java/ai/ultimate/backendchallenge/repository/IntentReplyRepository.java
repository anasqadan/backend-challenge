package ai.ultimate.backendchallenge.repository;

import ai.ultimate.backendchallenge.domain.IntentReply;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Intent reply repository.
 */
@Repository
public interface IntentReplyRepository extends MongoRepository<IntentReply, String> {

    /**
     * Find by name intent reply.
     *
     * @param name the name
     * @return the intent reply
     */
    IntentReply findByName(String name);
}