package ai.ultimate.backendchallenge.service;

import ai.ultimate.backendchallenge.domain.IntentReply;
import ai.ultimate.backendchallenge.exception.UltimateApiException;

/**
 * The interface Ultimate service.
 */
public interface IntentReplyService {

    /**
     * Gets response.
     *
     * @param botId   the bot id
     * @param message the message
     * @return the response
     */
    IntentReply getIntentReply(String botId, String message) throws UltimateApiException;
}
