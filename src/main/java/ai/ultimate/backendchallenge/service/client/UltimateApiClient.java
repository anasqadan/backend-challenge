package ai.ultimate.backendchallenge.service.client;

import ai.ultimate.backendchallenge.domain.client.ClientRequest;
import ai.ultimate.backendchallenge.domain.client.ClientResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * The interface Number Pooling api client.
 *
 * @author AQuedan
 */
@FeignClient(name = "number-pooling", url = "${client.ultimate.url}")
public interface UltimateApiClient {

    @PostMapping(value = "/intents")
    ClientResponse getIntent(@RequestHeader(value = "Authorization") String authorizationHeader, @RequestBody ClientRequest clientRequest);

}
