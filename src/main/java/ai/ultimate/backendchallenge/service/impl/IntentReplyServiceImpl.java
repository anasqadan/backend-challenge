package ai.ultimate.backendchallenge.service.impl;

import ai.ultimate.backendchallenge.domain.IntentReply;
import ai.ultimate.backendchallenge.domain.client.ClientRequest;
import ai.ultimate.backendchallenge.domain.client.ClientResponse;
import ai.ultimate.backendchallenge.domain.client.Intent;
import ai.ultimate.backendchallenge.exception.UltimateApiException;
import ai.ultimate.backendchallenge.repository.IntentReplyRepository;
import ai.ultimate.backendchallenge.service.IntentReplyService;
import ai.ultimate.backendchallenge.service.client.UltimateApiClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Optional;

/**
 * The type Ultimate service.
 */
@Log4j2
@Service
public class IntentReplyServiceImpl implements IntentReplyService {

    private final UltimateApiClient ultimateApiClient;

    private final IntentReplyRepository intentReplyRepository;

    private final String apiKey;

    /**
     * Instantiates a new Ultimate service.
     *
     * @param ultimateApiClient     the ultimate api client
     * @param intentReplyRepository the intent reply repository
     * @param apiKey                the api key
     */
    IntentReplyServiceImpl(UltimateApiClient ultimateApiClient, IntentReplyRepository intentReplyRepository, @Value("${client.ultimate.api-key}") String apiKey) {
        this.ultimateApiClient = ultimateApiClient;
        this.intentReplyRepository = intentReplyRepository;
        this.apiKey = apiKey;
    }

    @Override
    public IntentReply getIntentReply(String botId, String message) throws UltimateApiException {

        ClientRequest clientRequest = new ClientRequest(botId, message);

        ClientResponse intent = ultimateApiClient.getIntent(apiKey, clientRequest);

        if (intent == null) {
            log.error("Ultimate API is not returning any intent matching to the following message {}", message);
            throw new UltimateApiException("No Intent was found!");
        }

        Optional<Intent> highestConfidenceIntent = intent.getIntents().stream().sorted(Comparator.comparingDouble(Intent::getConfidence).reversed()).findFirst();

        if (!highestConfidenceIntent.isPresent()) {
            log.error("No returned intent matching you message: {}", message);
            return null;
        }
        return intentReplyRepository.findByName(highestConfidenceIntent.get().getName());
    }
}
