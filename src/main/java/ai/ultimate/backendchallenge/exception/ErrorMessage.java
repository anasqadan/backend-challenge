package ai.ultimate.backendchallenge.exception;

import java.util.Date;

/**
 * The type Error message.
 */
public class ErrorMessage {
    private int statusCode;
    private Date timestamp;
    private String message;
    private String description;

    /**
     * Instantiates a new Error message.
     *
     * @param statusCode  the status code
     * @param timestamp   the timestamp
     * @param message     the message
     * @param description the description
     */
    public ErrorMessage(int statusCode, Date timestamp, String message, String description) {
        this.statusCode = statusCode;
        this.timestamp = timestamp;
        this.message = message;
        this.description = description;
    }

    /**
     * Gets status code.
     *
     * @return the status code
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Gets timestamp.
     *
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }
}