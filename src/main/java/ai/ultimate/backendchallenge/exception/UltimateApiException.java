package ai.ultimate.backendchallenge.exception;

public class UltimateApiException extends Exception {

    public UltimateApiException() {
        super();
    }

    public UltimateApiException(String message) {
        super(message);
    }

    public UltimateApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public UltimateApiException(Throwable cause) {
        super(cause);
    }

    protected UltimateApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
