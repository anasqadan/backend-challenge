package ai.ultimate.backendchallenge.exception;

import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * The type Controller exception handler.
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    /**
     * Resource not found exception response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorMessage> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
    }

    /**
     * Global exception handler response entity.
     *
     * @param ex      the ex
     * @param request the request
     * @return the response entity
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> globalExceptionHandler(Exception ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Handle feign status exception string.
     *
     * @param ex       the e
     * @param response the response
     * @return the string
     */
    @ExceptionHandler(FeignException.class)
    public ResponseEntity<ErrorMessage> handleFeignStatusException(FeignException ex, HttpServletResponse response) {
        ErrorMessage message = new ErrorMessage(
                response.getStatus(),
                new Date(),
                ex.getMessage(),
                "feign Error!");
        return new ResponseEntity<>(message, HttpStatus.valueOf(response.getStatus()));
    }

    @ExceptionHandler(UltimateApiException.class)
    public ResponseEntity<ErrorMessage> handleUltimateApiException(UltimateApiException ex, HttpServletResponse response) {
        ErrorMessage message = new ErrorMessage(
                response.getStatus(),
                new Date(),
                ex.getMessage(),
                "Ultimate API Error!");
        return new ResponseEntity<>(message, HttpStatus.valueOf(response.getStatus()));
    }
}