package ai.ultimate.backendchallenge.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * The type Training data.
 */
@Data
@NoArgsConstructor
public class TrainingData {

    private List<Message> messages;
    private List<Message> expressions;
    private Integer expressionCount;


    /**
     * Instantiates a new Training data.
     *
     * @param messages the messages
     */
    public TrainingData(List<Message> messages) {
        this.messages = messages;
    }

    /**
     * Instantiates a new Training data.
     *
     * @param expressions     the expressions
     * @param expressionCount the expression count
     */
    public TrainingData(List<Message> expressions, Integer expressionCount) {
        this.expressions = expressions;
        this.expressionCount = expressionCount;
    }


}