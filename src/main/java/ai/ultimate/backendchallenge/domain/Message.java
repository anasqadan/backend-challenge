package ai.ultimate.backendchallenge.domain;

import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * The type Message.
 */
@Data
@AllArgsConstructor
public class Message {

    private String id;
    private String text;

}