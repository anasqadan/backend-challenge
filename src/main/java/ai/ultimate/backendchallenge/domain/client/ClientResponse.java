package ai.ultimate.backendchallenge.domain.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * The type Client response.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientResponse {

    private List<Intent> intents;
    private List<Object> entities;
}