package ai.ultimate.backendchallenge.domain.client;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The type Intent.
 */
@Data
@AllArgsConstructor
public class Intent {

    private Double confidence;
    private String name;

}

