package ai.ultimate.backendchallenge.domain.client;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The type Client request.
 */
@Data
@AllArgsConstructor
public class ClientRequest {

    private String botId;
    private String message;

}
