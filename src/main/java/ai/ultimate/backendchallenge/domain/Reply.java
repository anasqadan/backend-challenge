package ai.ultimate.backendchallenge.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The type Reply.
 */
@Data
@AllArgsConstructor
public class Reply {

    private String id;
    private String text;

}