package ai.ultimate.backendchallenge.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;


/**
 * The type Intent reply.
 */
@Document(collection = "replies")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IntentReply implements Serializable {

    @Id
    private String id;

    @Field(name = "name")
    private String name;

    @Field(name = "description")
    private String description;

    @Field(name = "trainingData")
    private TrainingData trainingData;

    @Field(name = "reply")
    private Reply reply;

}
