package ai.ultimate.backendchallenge.config;

import ai.ultimate.backendchallenge.domain.IntentReply;
import ai.ultimate.backendchallenge.domain.Message;
import ai.ultimate.backendchallenge.domain.Reply;
import ai.ultimate.backendchallenge.domain.TrainingData;
import ai.ultimate.backendchallenge.repository.IntentReplyRepository;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 * The type Mongo db config.
 */
@Configuration
@EnableMongoRepositories(basePackageClasses = IntentReply.class)
public class MongoDBConfig {

    @Value("${spring.data.mongodb.uri}")
    private String mongoUri;

    /**
     * Mongo client mongo client.
     *
     * @return the mongo client
     */
    @Bean
    public MongoClient mongoClient() {
        CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        return MongoClients.create(MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(mongoUri))
                .codecRegistry(codecRegistry)
                .build());
    }

    /**
     * Command line runner command line runner.
     *
     * @param intentReplyRepository the intent reply repository
     * @return the command line runner
     */
    @Bean
    CommandLineRunner commandLineRunner(IntentReplyRepository intentReplyRepository) {
        return strings -> {

            Message intent1_message1 = new Message("6399fd6989984c7b871c6301744b0af5", "Hello");
            Message intent1_message2 = new Message("68bafebc2a2e4843a56a221c2ceb12ed", "Hi");
            Message intent1_message3 = new Message("b2a3208dc801432992812638368e0668", "Good morning!");
            TrainingData intent1_trainingData = new TrainingData(Arrays.asList(intent1_message1, intent1_message2, intent1_message3));

            Reply intent1_reply = new Reply("f35d7e0936a44102bac9cb96c81eec3b", "Hello :) How can I help you?");

            intentReplyRepository.save(new IntentReply("34d7831e137a4016a55f98926800a643", "Greeting", "The visitor says hello.", intent1_trainingData, intent1_reply));


            Message intent2_message1 = new Message("6bb364d2e3364e03b4ca30c6e46ea1dd", "Thanks, bye!");
            Message intent2_message2 = new Message("2bc38310a4d1450f9e7c9e7903e458b9", "Goodbye!");
            Message intent2_message3 = new Message("611c935266c1402ab76f5235827370f8", "See you");
            TrainingData intent2_trainingData = new TrainingData(Arrays.asList(intent2_message1, intent2_message2, intent2_message3));

            Reply intent2_reply = new Reply("9ba88034a89e4fdbb532bdb092717fa1", "Goodbye, have a nice day!");

            intentReplyRepository.save(new IntentReply("b6ec3deac5f94500aef55d9c410e37f7", "Goodbye", "The visitor says goodbye.", intent2_trainingData, intent2_reply));


            Message intent3_message1 = new Message("13039d5bff7b4e3c951c716826f3598d", "Yeah");
            Message intent3_message2 = new Message("c677b5a2efe44bd8a92e2c35124a6ab1", "yep!");
            Message intent3_message3 = new Message("f990846d295a4b2289439efd8abedb7b", "yes, please");
            TrainingData intent3_trainingData = new TrainingData(Arrays.asList(intent3_message1, intent3_message2, intent3_message3));

            Reply intent3_reply = new Reply("3c9029f14fd74a6aac3a571d403bab35", "Great!");

            intentReplyRepository.save(new IntentReply("61e218983f8b49f79405e8cf22992e61", "Affirmative", "The visitor confirms that something is true / correct.", intent3_trainingData, intent3_reply));

            Message intent4_message1 = new Message("438303d11e3a4833973c7319cdf23275", "No thanks!");
            Message intent4_message2 = new Message("5db9342ebc644d6c824911323d23e568", "nope");
            Message intent4_message3 = new Message("060224b8c36347f79d11bb6a73a078f6", "please don't");
            TrainingData intent4_trainingData = new TrainingData(Arrays.asList(intent4_message1, intent4_message2, intent4_message3));

            Reply intent4_reply = new Reply("133957c37f954d6a8c0b721fbc3b652a", "Alright, please let me know if I can help you with anything else!!");

            intentReplyRepository.save(new IntentReply("629ebabd5d714900bbc7eb2c9eceb927", "Negative", "The visitor confirms that they don't need help / something is not true or similar.", intent4_trainingData, intent4_reply));


            Message intent5_message1 = new Message("71dbc302036d4257bbb6c477e0e0a7fc", "Brilliant! Thanks!");
            Message intent5_message2 = new Message("0fa23ff354fb41b29da47ecd7fbbdaee", "thx");
            Message intent5_message3 = new Message("9d8a7691475e45c0b06304b34721ce38", "thank you!");
            TrainingData intent5_trainingData = new TrainingData(Arrays.asList(intent5_message1, intent5_message2, intent5_message3));

            Reply intent5_reply = new Reply("17134b01d2e343bc81e48fad4ec2ca00", "It was a pleasure to be of help :)");

            intentReplyRepository.save(new IntentReply("f83a8f67dd8e4eef8c743a0f324aeca0", "Thank you", "The visitor says thank you.", intent5_trainingData, intent5_reply));


            Message intent6_message1 = new Message("7b943480157041ae971c019a4f60b0f4", "Are u human?");
            Message intent6_message2 = new Message("0b06b67653e749e9a23df1a504962f39", "Are you a bot?");
            Message intent6_message3 = new Message("037f9c3ef28540078527349c1c56e1d9", "Is this a robot?");
            TrainingData intent6_trainingData = new TrainingData(Arrays.asList(intent6_message1, intent6_message2, intent6_message3));

            Reply intent6_reply = new Reply("7d169954803d4bb4a40588c53eda620c", "I'm an AI bot, and I'm here to help you with your questions.");

            intentReplyRepository.save(new IntentReply("f505432f6dcd40548983e4eab2675429", "Are you a bot?", "The visitor wants to know if they are talking to a bot or a human.", intent6_trainingData, intent6_reply));

            Message intent7_message1 = new Message("78f4760adc1545229e32b0b453d20362", "Can I talk to a human?");
            Message intent7_message2 = new Message("71d413bc26cd4a85ac98f99198c324d0", "I want to speak to an agent.");
            Message intent7_message3 = new Message("27fa5c9224934952aa5dfe6f6a229ba5", "transfer to a human");
            Integer expressionCount = 59;
            TrainingData intent7_trainingData = new TrainingData(Arrays.asList(intent7_message1, intent7_message2, intent7_message3), expressionCount);

            Reply intent7_reply = new Reply("6ad4f9d516c44eb8ad765f557ecc3ca6", "Let me transfer you to the first available agent.");

            intentReplyRepository.save(new IntentReply("0edf4a33873d482f857bfa0a5c16b7ce", "I want to speak with a human", "The visitor wants to speak to a human agent.", intent7_trainingData, intent7_reply));

            Message intent8_message1 = new Message("ec7e980908c84822b18e331e2496045a", "I can't sign in");
            Message intent8_message2 = new Message("f81537f3f69b4b729fe1c71f83e7e25e", "The login is giving me an error.");
            Message intent8_message3 = new Message("d748c7a9bc124abc9fa3a3147fb11651", "Can you help me change my password, can't log in!");
            TrainingData intent8_trainingData = new TrainingData(Arrays.asList(intent8_message1, intent8_message2, intent8_message3), null);

            Reply intent8_reply = new Reply("0edf4a33873d482f857bfa0a5c16b7ce", "Oh no! Please give me your email and I will fix it.");

            intentReplyRepository.save(new IntentReply("f505432f6dcd40548983e4eab1675429", "Login Problems", "The visitor has trouble logging in.", intent8_trainingData, intent8_reply));

            Message intent9_message1 = new Message("5216d8a096f44cf681b54cac19b8f29a", "Where do I open a new account?");
            Message intent9_message2 = new Message("bd959fc688404f188bc23dd1abefc772", "how to start an account ");
            Message intent9_message3 = new Message("a34e39f190f0491f8953a6a215261f2e", "i need help creating an account");
            TrainingData intent9_trainingData = new TrainingData(Arrays.asList(intent9_message1, intent9_message2, intent9_message3));

            Reply intent9_reply = new Reply("d1bf934c18634586962758fb98ff44a1", "Please follow these instructions \"LINK\" to open a new account.");

            intentReplyRepository.save(new IntentReply("29a0d3b7cecc4fe5955f6c5c30fbcf6b", "Open or close account", "The visitor wants to create a new account or close an existing one.", intent9_trainingData, intent9_reply));
        };
    }
}