# README #

### What is this repository for? ###

This is to retrieve an intent from Ultimate API and check the intent replies if it exists on Mongodb.

### Configuration needed to be done over the application before starting ###

* Check that port 9090 is available in your machine, otherwise, try to change the port of this application to another free port
* Check if MongoD connection that mentioned in the application.properties matches yours, otherwise; change them to make them proper

### Swagger UI? ###

To reach Swagger API documentation ; you can follow the following link http:localhost:${port}/swagger-ui/index.html 

* in default case here 
http://localhost:9090/swagger-ui/index.html

### Who do I talk to? ###

* This is Repository added and pushed by @ Anas Quedan
if you have any comments, feel free to reach me out via [Anas Quedan Email](mailto:anas.quedan@gmail.com) 